package test.ex.u.notificationreminder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangeTextNotifications extends AppCompatActivity
                                     implements View.OnClickListener {

    //public final String KEY = getString(R.string.INTENT_FILTER_KEY);
//    public String SEND_MESSAGE_ACTION = getString(R.string.BROADCAST_ACTION);
    Button buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_text_notifications);

        buttonSave = (Button)findViewById(R.id.btn_save);
        buttonSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
    String newMessage = ((EditText)findViewById(R.id.editText)).getText().toString();
        Intent intent = new Intent();
        intent.setAction(getString(R.string.BROADCAST_ACTION));
        Bundle bundle = new Bundle();
        bundle.putString("test.ex.u.notificationreminder.KEY", newMessage);
        intent.putExtra("key",bundle);
        sendBroadcast(intent);

        buttonSave.setEnabled(false);

        Toast.makeText(getApplicationContext(),
                "текст сохранен", Toast.LENGTH_SHORT).show();
        finish();
    }
}

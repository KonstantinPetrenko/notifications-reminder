package test.ex.u.notificationreminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;


public class CreateNotifications {


    private int idNotifications = 1;
    Context context;

    public CreateNotifications(Context context) {
        this.context = context;
    }

    public void notifications(String message) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getString(R.string.remind))
                        .setContentText(message)
                        .setTicker(context.getString(R.string.new_notification))
                        .setDefaults(Notification.DEFAULT_ALL)
                        //.setDeleteIntent()
                        .setAutoCancel(true);

        Intent resultIntent = new Intent(context, ChangeTextNotifications.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder.addParentStack(ChangeTextNotifications.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager)
                        context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(idNotifications++, mBuilder.build());
    }
}



package test.ex.u.notificationreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BootBroadcastReceiver extends BroadcastReceiver {

    private final String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";

    @Override
    public   void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (action.equalsIgnoreCase(BOOT_ACTION))
            context.startService(new Intent(context, ServiceNotifications.class));
    }


}

package test.ex.u.notificationreminder;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class ServiceNotifications extends Service {

    private final String TAG = "ServiceNotifications";
    private final int REMINDER = 10 * 1000;
    String message = "пора пить чай";
    private Context context;
    private Timer mTimer;
    private final String MESSAGE_ACTION =
            "android.intent.action.test.ex.u.notificationreminder.ACTION";

//    TimerTaskRun timerTaskRun;
    BootBroadcastReceiver receiveNewText;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        context = getApplicationContext();
        Toast.makeText(context,
                "onCreate ServiceNotifications", Toast.LENGTH_SHORT).show();

        receiveNewText = new BootBroadcastReceiver() {

            public void onReceive(Context context, Intent intent) {
               //String message = intent.getStringExtra("test.ex.u.notificationreminder.KEY");
                message = intent.getBundleExtra("key")
                        .getString("test.ex.u.notificationreminder.KEY");

                Toast.makeText(context,
                        message + " New sms", Toast.LENGTH_SHORT).show();
            }
        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter action = new IntentFilter(MESSAGE_ACTION);
        // регистрируем (включаем) BroadcastReceiver
        registerReceiver(receiveNewText, action);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        timerTaskRun = new TimerTaskRun(context);

        Toast.makeText(context, "onStartCommand", Toast.LENGTH_SHORT).show();

        if (mTimer != null)
            mTimer.cancel();

        mTimer = new Timer();

        int DELAY_NOTIFICATION_PERIOD = 10000;
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                new CreateNotifications(context).notifications(message);
            }
        }, DELAY_NOTIFICATION_PERIOD, REMINDER);

        Toast.makeText(context, "Сервис запущен", Toast.LENGTH_SHORT).show();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        unregisterReceiver(receiveNewText);
                Toast.makeText(context,
                        "Сервис остановлен", Toast.LENGTH_SHORT).show();
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
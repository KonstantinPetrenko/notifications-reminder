package test.ex.u.notificationreminder;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;

public class StartService extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private final String TAG = "StartService";
    private static final String SWITCH_POSITION = "switch_position";
    private static final String MY_SETTINGS = "settings";

    Switch serviceStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_service);

        serviceStart = (Switch) findViewById(R.id.switch_Service);
        serviceStart.setChecked(LoadPreferences(SWITCH_POSITION));
        serviceStart.setOnCheckedChangeListener(this);

        Log.d(TAG, "onCreate");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(serviceStart.isChecked())
            startService(new Intent(this, ServiceNotifications.class));

         else
            stopService(new Intent(this, ServiceNotifications.class));

        SavePreferences(SWITCH_POSITION, serviceStart.isChecked());
    }

    private void SavePreferences(String key, Boolean value) {
        SharedPreferences sharedPreferences = getSharedPreferences(
                MY_SETTINGS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private Boolean LoadPreferences(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(MY_SETTINGS, MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }
}